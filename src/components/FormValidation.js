import React, { Component } from "react";
import { Button, Form, FormGroup, Label, Input, Alert } from 'reactstrap';
import ConfirmationModal from "./ConfirmationModal"
import 'bootstrap/dist/css/bootstrap.css'


export default class FormValidation extends Component {
    constructor(props) {
        super(props)
        this.state = {
            fullname: "",
            email: "",
            country: "UK",
            errors: {
                fullname: "",
                email: "",
                country: ""
            },
            showModal: false
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleFormAccept = this.handleFormAccept.bind(this);
        this.handleFormReject = this.handleFormReject.bind(this);
    }


    handleChange = (event) => {
        event.preventDefault()
        const { name, value } = event.target;
        const errors = this.state.errors
        let re;
        switch (name) {
            case "fullname":
                re = /^[a-z ,.'-]+$/i
                const fullname = value.trim();
                const fullnameLength = fullname.split("").length
                errors.fullname = fullnameLength === 0 ? "Please enter a valid name" : (fullnameLength < 10) ? "Name should be atleast 10 characters long" : !re.test(fullname) ? "Name can only contain alphabets" : ""
                break;

            case "email":
                re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                const email = value.trim();
                const emailLength = email.split("").length
                errors.email = email && emailLength < 5 ? "Please enter a valid email address" : !re.test(email) ? "Email must contain @ and a domain" : ""
                break;

            case "country":
                errors.country = ""
                break;
            default:
                break;
        }
        this.setState(() => ({ errors, [name]: value }))
    }

    handleFormSubmit = (event) => {
        event.preventDefault()
        const isEmailExists = this.state.email
        const isNameExists = this.state.fullname
        const errors = this.state.errors
        errors.email = (isEmailExists) ? (errors.email ? errors.email : "") : "Please enter a valid email address"
        errors.fullname = (isNameExists) ? (errors.fullname ? errors.fullname : "") : "Please enter a valid name"

        this.setState(() => ({
             errors,showModal: !this.state.errors.email && !this.state.errors.fullname
        }))

        // if (!this.state.errors.email && !this.state.errors.fullname) {
        //     // alert(`Hi, I am ${this.state.fullname}, my email is ${this.state.email} and I live in ${this.state.country}`)
        //     this.setState({ showModal: true })
        // }
    }


    handleFormAccept = () => {
        this.setState({ showModal: false })
        console.log("form accepted")
        // submit the form here
    }

    handleFormReject = () => {
        this.setState({ showModal: false })
        console.log("form rejected")
        // form rejected, keep the form details as it is
    }

    render() {
        const isDisabled = !(!this.state.errors.fullname && this.state.fullname && !this.state.errors.email && this.state.email)
        return (
            <div>
                <h3 className="text-center">Registration</h3>
                <Form id="userform" onSubmit={this.handleFormSubmit}>
                    <FormGroup>
                        <Label>Full Name</Label>
                        <Input type="text" name="fullname" id="userName" placeholder="Full Name" onChange={this.handleChange} value={this.state.fullname} />
                        {!!this.state.errors.fullname && <Alert fade={true} className="mt-2" color="danger">{this.state.errors.fullname}</Alert>}
                    </FormGroup>
                <FormGroup>
                    <Label>Email</Label>
                    <Input type="email" placeholder="Email" name="email" id="userEmail" onChange={this.handleChange} value={this.state.email} >
                    </Input>
                    {!!this.state.errors.email && <Alert fade={true} className="mt-2" color="danger">{this.state.errors.email}</Alert>}
                </FormGroup>
                <FormGroup>
                    <Label for="countrySelect">Select</Label>
                    <Input type="select" name="country" id="countrySelect" value={this.state.country} onChange={this.handleChange}>
                        <option value="India">India</option>
                        <option value="USA">USA</option>
                        <option value="UK">UK</option>
                        <option value="Canada">Canada</option>
                    </Input>

                </FormGroup>
                <Button className="btn-lg btn-block" color={isDisabled ? "secondary" : "success"} disabled={isDisabled} type="submit">Submit</Button>

                <ConfirmationModal
                    showModal={this.state.showModal}
                    handleFormAccept={this.handleFormAccept}
                    handleFormReject={this.handleFormReject}
                />
                </Form>
            </div >

        );
    }
}
