
import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';


const ConfirmationModal = (props) => {
    const { handleFormAccept, handleFormReject, showModal } = props;
    return (
        <div>
            <Modal isOpen={showModal} toggle={handleFormReject} centered={false} size="md" >
                <ModalHeader close={(<button className="close" onClick={handleFormReject}>&times;</button>)}>Confirmation </ModalHeader>
                <ModalBody>
                    Are you sure, you want to submit?
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={handleFormAccept}>Yes</Button>{' '}
                    <Button color="secondary" onClick={handleFormReject}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

export default ConfirmationModal;

