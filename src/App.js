import React from 'react';
// import logo from './logo.svg';
import FormValidation from './components/FormValidation'
import './App.css';

function App() {
  return (
    <div className="App">
     <FormValidation />
    </div>
  );
}

export default App;
